# Overview
This project contains a Dockerfile & CI YAML file to build an ARM64 based Minio container

# How to Use
Requires: 
* Docker buildx -> https://docs.docker.com/buildx/working-with-buildx/