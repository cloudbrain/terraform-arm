# # Dockerfile for Terraform ARM64 container

# FROM python:slim
# MAINTAINER Kelly Hair <khair@gitlab.com>

# # Install base packages
# RUN apt-get update
# RUN apt-get upgrade -y
# RUN apt-get install curl wget bash unzip git -y

# # Install terraform

# WORKDIR /tmp
# RUN export latest_version=`curl https://checkpoint-api.hashicorp.com/v1/check/terraform | python -mjson.tool | grep current_version | awk '{print $2}' | sed s/\"\//g | sed s/,//g` && \
#     export version=$latest_version && \
#     echo $version && \
#     wget "https://releases.hashicorp.com/terraform/${version}/terraform_${version}_linux_amd64.zip" && \
#     unzip *.zip && \
#     mv terraform /usr/bin/terraform

FROM golang:alpine
MAINTAINER "HashiCorp Terraform Team <terraform@hashicorp.com>"

ENV TERRAFORM_VERSION=0.12.24

RUN apk add --update git bash openssh curl

ENV TF_DEV=true
ENV TF_RELEASE=true

WORKDIR $GOPATH/src/github.com/hashicorp/terraform
RUN git clone https://github.com/hashicorp/terraform.git ./ && \
    git checkout v${TERRAFORM_VERSION} && \
    /bin/bash scripts/build.sh

WORKDIR $GOPATH
ENTRYPOINT ["terraform"]